USE [master]
GO
/****** Object:  Database [StudentDB]    Script Date: 2019-07-08 06:56:38 ******/
CREATE DATABASE [StudentDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'StudentMaster', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\StudentMaster.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'StudentMaster_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\StudentMaster_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [StudentDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StudentDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StudentDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StudentDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StudentDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StudentDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StudentDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [StudentDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [StudentDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StudentDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StudentDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StudentDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StudentDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StudentDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StudentDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StudentDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StudentDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [StudentDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StudentDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StudentDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StudentDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StudentDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StudentDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [StudentDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StudentDB] SET RECOVERY FULL 
GO
ALTER DATABASE [StudentDB] SET  MULTI_USER 
GO
ALTER DATABASE [StudentDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StudentDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [StudentDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [StudentDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [StudentDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'StudentDB', N'ON'
GO
USE [StudentDB]
GO
/****** Object:  Table [dbo].[GenderLookup]    Script Date: 2019-07-08 06:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GenderLookup](
	[GenderID] [int] NOT NULL,
	[GenderName] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_GenderLookup] PRIMARY KEY CLUSTERED 
(
	[GenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchoolGradeLookup]    Script Date: 2019-07-08 06:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolGradeLookup](
	[GradeID] [int] IDENTITY(1,1) NOT NULL,
	[SequenceNo] [int] NOT NULL,
	[GradeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_SchoolGradeLookup] PRIMARY KEY CLUSTERED 
(
	[GradeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchoolMaster]    Script Date: 2019-07-08 06:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolMaster](
	[SchoolID] [int] IDENTITY(1,1) NOT NULL,
	[SchoolName] [nvarchar](200) NOT NULL,
	[SchoolCode] [nvarchar](20) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_SchoolMaster] PRIMARY KEY CLUSTERED 
(
	[SchoolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentMaster]    Script Date: 2019-07-08 06:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentMaster](
	[StudentID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[OtherName] [varchar](150) NULL,
	[LastName] [varchar](100) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[GenderID] [int] NULL,
	[SchoolID] [int] NOT NULL,
	[GradeID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateUserID] [int] NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyUserID] [int] NOT NULL,
 CONSTRAINT [PK_StudentMaster] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 2019-07-08 06:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[EmailID] [nvarchar](200) NULL,
	[ContactNo] [nvarchar](10) NULL,
	[Password] [nvarchar](200) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK__Users__3214EC070F975522] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[GenderLookup] ([GenderID], [GenderName]) VALUES (0, N'Not Given')
INSERT [dbo].[GenderLookup] ([GenderID], [GenderName]) VALUES (1, N'Female')
INSERT [dbo].[GenderLookup] ([GenderID], [GenderName]) VALUES (2, N'Male')
SET IDENTITY_INSERT [dbo].[SchoolGradeLookup] ON 

INSERT [dbo].[SchoolGradeLookup] ([GradeID], [SequenceNo], [GradeName]) VALUES (2, 2, N'GR11')
INSERT [dbo].[SchoolGradeLookup] ([GradeID], [SequenceNo], [GradeName]) VALUES (3, 3, N'GR12')
INSERT [dbo].[SchoolGradeLookup] ([GradeID], [SequenceNo], [GradeName]) VALUES (4, 1, N'GR10')
SET IDENTITY_INSERT [dbo].[SchoolGradeLookup] OFF
SET IDENTITY_INSERT [dbo].[SchoolMaster] ON 

INSERT [dbo].[SchoolMaster] ([SchoolID], [SchoolName], [SchoolCode], [CreatedBy], [CreatedDate], [Active]) VALUES (1, N'Skye High', N'SKH', 1, CAST(N'2019-07-07 14:31:28.177' AS DateTime), 1)
INSERT [dbo].[SchoolMaster] ([SchoolID], [SchoolName], [SchoolCode], [CreatedBy], [CreatedDate], [Active]) VALUES (3, N'Park High', N'PH', 1, CAST(N'2019-07-07 17:03:41.310' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[SchoolMaster] OFF
SET IDENTITY_INSERT [dbo].[StudentMaster] ON 

INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (1, N'Malamba', N'', N'Nemavhadwe', CAST(N'1997-04-29' AS Date), 2, 1, 2, 1, CAST(N'2019-07-08 03:23:55.033' AS DateTime), 1, CAST(N'2019-07-08 03:23:55.033' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (2, N'Talenta', NULL, N'Matiane', CAST(N'1998-01-25' AS Date), 1, 1, 4, 1, CAST(N'2019-07-08 06:30:13.917' AS DateTime), 1, CAST(N'2019-07-08 06:30:13.917' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (3, N'Nyandano ', NULL, N'Madiba', CAST(N'1998-07-14' AS Date), 2, 1, 4, 1, CAST(N'2019-07-08 06:31:03.747' AS DateTime), 1, CAST(N'2019-07-08 06:31:03.747' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (4, N'Tovhowani ', NULL, N'Mulovhedzi', CAST(N'1998-02-21' AS Date), 2, 1, 4, 1, CAST(N'2019-07-08 06:32:12.350' AS DateTime), 1, CAST(N'2019-07-08 06:32:12.350' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (5, N'Siphokazi ', NULL, N'Hlalukana', CAST(N'1996-01-20' AS Date), 1, 1, 3, 1, CAST(N'2019-07-08 06:33:46.620' AS DateTime), 1, CAST(N'2019-07-08 06:33:46.620' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (6, N'Duncan ', NULL, N'Smith', CAST(N'1997-11-26' AS Date), 2, 3, 2, 1, CAST(N'2019-07-08 06:34:37.760' AS DateTime), 1, CAST(N'2019-07-08 06:34:37.760' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (7, N'Mpho ', NULL, N'Ramagoma', CAST(N'1967-01-02' AS Date), 1, 3, 2, 1, CAST(N'2019-07-08 06:35:28.833' AS DateTime), 1, CAST(N'2019-07-08 06:35:28.833' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (8, N'Eunice ', NULL, N'Bosch', CAST(N'1997-05-24' AS Date), 1, 3, 2, 1, CAST(N'2019-07-08 06:36:13.813' AS DateTime), 1, CAST(N'2019-07-08 06:36:13.813' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (10, N'Thendo ', N'Yeolaine ', N'Masutha', CAST(N'1996-07-17' AS Date), 0, 3, 3, 1, CAST(N'2019-07-08 06:42:01.853' AS DateTime), 1, CAST(N'2019-07-08 06:42:01.853' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (11, N'Nomaan ', NULL, N'Mulla', CAST(N'1996-08-08' AS Date), 0, 3, 3, 1, CAST(N'2019-07-08 06:42:53.493' AS DateTime), 1, CAST(N'2019-07-08 06:42:53.493' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[StudentMaster] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [UserName], [FullName], [EmailID], [ContactNo], [Password], [CreatedBy], [CreatedDate], [Active]) VALUES (1, N'Admin', N'Test User', N'admin@admin.com', N'0612080529', N'tttdoybuFsAnWJYAfwOUqg==', 1, CAST(N'2019-07-07 13:43:08.607' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[SchoolMaster]  WITH CHECK ADD  CONSTRAINT [FK_SchoolMaster_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[SchoolMaster] CHECK CONSTRAINT [FK_SchoolMaster_User]
GO
ALTER TABLE [dbo].[StudentMaster]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaster_GenderLookup] FOREIGN KEY([GenderID])
REFERENCES [dbo].[GenderLookup] ([GenderID])
GO
ALTER TABLE [dbo].[StudentMaster] CHECK CONSTRAINT [FK_StudentMaster_GenderLookup]
GO
ALTER TABLE [dbo].[StudentMaster]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaster_SchoolGradeLookup1] FOREIGN KEY([GradeID])
REFERENCES [dbo].[SchoolGradeLookup] ([GradeID])
GO
ALTER TABLE [dbo].[StudentMaster] CHECK CONSTRAINT [FK_StudentMaster_SchoolGradeLookup1]
GO
ALTER TABLE [dbo].[StudentMaster]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaster_SchoolMaster] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[SchoolMaster] ([SchoolID])
GO
ALTER TABLE [dbo].[StudentMaster] CHECK CONSTRAINT [FK_StudentMaster_SchoolMaster]
GO
ALTER TABLE [dbo].[StudentMaster]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaster_User] FOREIGN KEY([CreateUserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[StudentMaster] CHECK CONSTRAINT [FK_StudentMaster_User]
GO
ALTER TABLE [dbo].[StudentMaster]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaster_User1] FOREIGN KEY([ModifyUserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[StudentMaster] CHECK CONSTRAINT [FK_StudentMaster_User1]
GO
/****** Object:  StoredProcedure [dbo].[sp_save_student]    Script Date: 2019-07-08 06:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_save_student]
(
	@StudentID int = 0 output,
	@FirstName nvarchar(100),
	@OtherName nvarchar(150),
	@LastName nvarchar(100),
	@DateOfBirth DATETIME,
	@GenderID int = NULL,
	@SchoolID int,
	@GradeID int,
	@Active BIT,
	@CreateUserID INT = 0
)
AS

BEGIN
	DECLARE @ReturnValue INT
	IF ISNULL( @StudentID, 0 ) = 0 OR NOT exists(select * from StudentMaster where StudentID = @StudentID)
		BEGIN
			INSERT INTO StudentMaster( FirstName, OtherName, LastName, DateOfBirth, GenderID,
			 SchoolID, GradeID, Active, CreateDate, CreateUserID, ModifyDate, ModifyUserID)
			VALUES ( @FirstName, @OtherName, @LastName, @DateOfBirth, @GenderID,
			@SchoolID, @GradeID, @Active, GETDATE(), @CreateUserID, GETDATE(), @CreateUserID)
			 
			 SELECT @ReturnValue = SCOPE_IDENTITY()		  
		END
	ELSE
		BEGIN
			UPDATE StudentMaster SET
				FirstName = @FirstName,
				OtherName = @OtherName,
				LastName = @LastName,
				DateOfBirth = @DateOfBirth,
				GenderID = @GenderID,
				SchoolID = @SchoolID,
				GradeID = @GradeID,
				Active = @Active,
				ModifyDate = GETDATE(),				
				ModifyUserID = @CreateUserID
			WHERE StudentID = @StudentID

			SELECT @ReturnValue = @StudentID
		END
END
IF (@@ERROR != 0)
BEGIN
	RETURN - 1
END
ELSE
BEGIN
	RETURN @ReturnValue
END


GO
USE [master]
GO
ALTER DATABASE [StudentDB] SET  READ_WRITE 
GO
