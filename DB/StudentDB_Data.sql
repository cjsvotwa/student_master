USE [StudentDB]
GO
SET IDENTITY_INSERT [dbo].[SchoolGradeLookup] ON 

INSERT [dbo].[SchoolGradeLookup] ([GradeID], [SequenceNo], [GradeName]) VALUES (2, 2, N'GR11')
INSERT [dbo].[SchoolGradeLookup] ([GradeID], [SequenceNo], [GradeName]) VALUES (3, 3, N'GR12')
INSERT [dbo].[SchoolGradeLookup] ([GradeID], [SequenceNo], [GradeName]) VALUES (4, 1, N'GR10')
SET IDENTITY_INSERT [dbo].[SchoolGradeLookup] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [UserName], [FullName], [EmailID], [ContactNo], [Password], [CreatedBy], [CreatedDate], [Active]) VALUES (1, N'Admin', N'Test User', N'admin@admin.com', N'0612080529', N'tttdoybuFsAnWJYAfwOUqg==', 1, CAST(N'2019-07-07 13:43:08.607' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[SchoolMaster] ON 

INSERT [dbo].[SchoolMaster] ([SchoolID], [SchoolName], [SchoolCode], [CreatedBy], [CreatedDate], [Active]) VALUES (1, N'Skye High', N'SKH', 1, CAST(N'2019-07-07 14:31:28.177' AS DateTime), 1)
INSERT [dbo].[SchoolMaster] ([SchoolID], [SchoolName], [SchoolCode], [CreatedBy], [CreatedDate], [Active]) VALUES (3, N'Park High', N'PH', 1, CAST(N'2019-07-07 17:03:41.310' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[SchoolMaster] OFF
INSERT [dbo].[GenderLookup] ([GenderID], [GenderName]) VALUES (0, N'Not Given')
INSERT [dbo].[GenderLookup] ([GenderID], [GenderName]) VALUES (1, N'Female')
INSERT [dbo].[GenderLookup] ([GenderID], [GenderName]) VALUES (2, N'Male')
SET IDENTITY_INSERT [dbo].[StudentMaster] ON 

INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (1, N'Malamba', N'', N'Nemavhadwe', CAST(N'1997-04-29' AS Date), 2, 1, 2, 1, CAST(N'2019-07-08 03:23:55.033' AS DateTime), 1, CAST(N'2019-07-08 03:23:55.033' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (2, N'Talenta', NULL, N'Matiane', CAST(N'1998-01-25' AS Date), 1, 1, 4, 1, CAST(N'2019-07-08 06:30:13.917' AS DateTime), 1, CAST(N'2019-07-08 06:30:13.917' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (3, N'Nyandano ', NULL, N'Madiba', CAST(N'1998-07-14' AS Date), 2, 1, 4, 1, CAST(N'2019-07-08 06:31:03.747' AS DateTime), 1, CAST(N'2019-07-08 06:31:03.747' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (4, N'Tovhowani ', NULL, N'Mulovhedzi', CAST(N'1998-02-21' AS Date), 2, 1, 4, 1, CAST(N'2019-07-08 06:32:12.350' AS DateTime), 1, CAST(N'2019-07-08 06:32:12.350' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (5, N'Siphokazi ', NULL, N'Hlalukana', CAST(N'1996-01-20' AS Date), 1, 1, 3, 1, CAST(N'2019-07-08 06:33:46.620' AS DateTime), 1, CAST(N'2019-07-08 06:33:46.620' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (6, N'Duncan ', NULL, N'Smith', CAST(N'1997-11-26' AS Date), 2, 3, 2, 1, CAST(N'2019-07-08 06:34:37.760' AS DateTime), 1, CAST(N'2019-07-08 06:34:37.760' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (7, N'Mpho ', NULL, N'Ramagoma', CAST(N'1967-01-02' AS Date), 1, 3, 2, 1, CAST(N'2019-07-08 06:35:28.833' AS DateTime), 1, CAST(N'2019-07-08 06:35:28.833' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (8, N'Eunice ', NULL, N'Bosch', CAST(N'1997-05-24' AS Date), 1, 3, 2, 1, CAST(N'2019-07-08 06:36:13.813' AS DateTime), 1, CAST(N'2019-07-08 06:36:13.813' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (10, N'Thendo ', N'Yeolaine ', N'Masutha', CAST(N'1996-07-17' AS Date), 0, 3, 3, 1, CAST(N'2019-07-08 06:42:01.853' AS DateTime), 1, CAST(N'2019-07-08 06:42:01.853' AS DateTime), 1)
INSERT [dbo].[StudentMaster] ([StudentID], [FirstName], [OtherName], [LastName], [DateOfBirth], [GenderID], [SchoolID], [GradeID], [Active], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (11, N'Nomaan ', NULL, N'Mulla', CAST(N'1996-08-08' AS Date), 0, 3, 3, 1, CAST(N'2019-07-08 06:42:53.493' AS DateTime), 1, CAST(N'2019-07-08 06:42:53.493' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[StudentMaster] OFF
