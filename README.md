### =========== DB =========== <br>
There is a .bak and a script in the Data Folder<br>

## =========== Connection String =========== <br>
The connection string is only configured in appsettings.json<br>

### =========== System User =========== <br>
There is a users table; just for login
Username : admin
Password : 123456



### =========== Development Tools =========== <br>
Visual Studio 2017 Enterprise
MSSQL SERVER 2017 Enterprise

C# with .NET Core 2.2 (for Web API)
EF & Dapper
Angular 7.0
Bootstrap Framework


