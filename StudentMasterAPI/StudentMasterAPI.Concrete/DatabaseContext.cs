﻿using Microsoft.EntityFrameworkCore;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Concrete
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext( DbContextOptions<DatabaseContext> options ) : base( options )
        {

        }
        public DbSet<SchoolMaster> SchoolMaster { get; set; }
        public DbSet<StudentMaster> StudentMaster { get; set; }
        public DbSet<GenderLookup> GenderLookup { get; set; }
        public DbSet<SchoolGradeLookup> SchoolGradeLookup { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
