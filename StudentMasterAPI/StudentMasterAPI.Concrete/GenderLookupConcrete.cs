﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Concrete
{
    public class GenderLookupConcrete : IGenderLookup
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _configuration;
        public GenderLookupConcrete( DatabaseContext _context, IConfiguration _config )
        {
            this._context = _context;
            _configuration = _config;
        }
        public List<GenderLookup> GetGenderList()
        {
            try
            {
                var _result = ( from _Gender in _context.GenderLookup
                               select _Gender ).OrderBy( _x => _x.GenderID ).ToList();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetGenderList", _Ex.Message );
            }
            return null;
        }

        public GenderLookup GetGenderById( int _iGenderId )
        {
            try
            {
                var _result = ( from _Gender in _context.GenderLookup
                               where _Gender.GenderID == _iGenderId
                               select _Gender ).FirstOrDefault();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetGenderById", _Ex.Message );
            }
            return null;
        }
    }
}
