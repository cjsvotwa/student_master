﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Concrete
{
    public class SchoolGradeLookupConcrete : ISchoolGradeLookup
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _configuration;
        public SchoolGradeLookupConcrete( DatabaseContext _context, IConfiguration _config )
        {
            this._context = _context;
            _configuration = _config;
        }
        public List<SchoolGradeLookup> GetSchoolGradeList()
        {
            try
            {
                var _result = ( from _Grade in _context.SchoolGradeLookup
                               select _Grade 
                               ).OrderBy( _x => _x.SequenceNo ).ToList();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetSchoolGradeList", _Ex.Message );
            }
            return null;
        }

        public SchoolGradeLookup GetGradeById( int _iGradeId )
        {
            try
            {
                var _result = ( from _Grade in _context.SchoolGradeLookup
                               where _Grade.GradeID == _iGradeId
                               select _Grade ).FirstOrDefault();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetGradeById", _Ex.Message );
            }
            return null;
        }
    }
}
