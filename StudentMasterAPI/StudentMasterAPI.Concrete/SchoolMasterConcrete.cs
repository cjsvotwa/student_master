﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Concrete
{
    public class SchoolMasterConcrete : ISchoolMaster
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _config;
        public SchoolMasterConcrete( DatabaseContext _context, IConfiguration _config )
        {
            this._context = _context;
            this._config = _config;
        }
        public List<SchoolMaster> GetSchoolMasterList()
        {
            try
            {
                var _result = ( from _School in _context.SchoolMaster
                               select _School ).ToList();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetSchoolMasterList", _Ex.Message );
            }
            return null;
        }

        public SchoolMaster GetSchoolMasterbyId( int _iSchoolId )
        {
            try
            {
                var _result = ( from _School in _context.SchoolMaster
                               where _School.SchoolID == _iSchoolId
                               select _School ).FirstOrDefault();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetSchoolMasterbyId", _Ex.Message );
            }
            return null;
        }

        public bool CheckSchoolNameExists( string _sSchoolCode,  int _iCurId = 0 )
        {
            try
            {
                var _result = ( from _School in _context.SchoolMaster
                               where ( _School.SchoolCode == _sSchoolCode
                               && _School.SchoolID != _iCurId )
                               select _School ).Count();
                return ( _result > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "CheckSchoolNameExists", _Ex.Message );
            }
            return false;
        }

        public bool AddSchoolMaster( SchoolMaster SchoolMaster )
        {
            try
            {
                _context.SchoolMaster.Add( SchoolMaster );
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "AddSchoolMaster", _Ex.Message );
            }
            return false;
        }

        public bool UpdateSchoolMaster( SchoolMaster _SchoolMaster )
        {
            try
            {
                _context.Entry( _SchoolMaster ).Property( _x => _x.SchoolName ).IsModified = true;
                _context.Entry( _SchoolMaster ).Property( _x => _x.SchoolCode ).IsModified = true;
                _context.Entry( _SchoolMaster ).Property( _x => _x.Active ).IsModified = true;
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "UpdateSchoolMaster", _Ex.Message );
            }
            return false;
        }

        public bool DeleteSchool( int _iSchoolId )
        {
            try
            {
                var _SchoolMaster = ( from _School in _context.SchoolMaster
                                       where _School.SchoolID == _iSchoolId
                                       select _School ).FirstOrDefault();
                if ( _SchoolMaster != null )
                {
                    _context.SchoolMaster.Remove( _SchoolMaster );
                    return ( _context.SaveChanges() > 0 );
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "DeleteSchool", _Ex.Message );
            }
            return false;
        }

        public List<SchoolMaster> GetActiveSchoolMasterList()
        {
            try
            {
                var _result = ( from _School in _context.SchoolMaster
                               where _School.Active == true
                               select _School ).ToList();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetActiveSchoolMasterList", _Ex.Message );
            }
            return null;
        }
    }
}
