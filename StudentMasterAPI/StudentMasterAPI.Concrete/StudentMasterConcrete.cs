﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Concrete
{
    public class StudentMasterConcrete : IStudentMaster
    {
        private readonly IConfiguration _configuration;
        private readonly DatabaseContext _context;
        public StudentMasterConcrete( DatabaseContext context, IConfiguration configuration )
        {
            _context = context;
            _configuration = configuration;

        }
        public void InsertStudent( StudentMaster _Student )
        {
            try
            {
                using ( SqlConnection con = new SqlConnection( _configuration.GetConnectionString( "DatabaseConnection" ) ) )
                {
                    var paramater = new DynamicParameters();
                    paramater.Add( "@StudentID", _Student.StudentID );
                    paramater.Add( "@FirstName", _Student.FirstName );
                    paramater.Add( "@OtherName", _Student.OtherName );
                    paramater.Add( "@LastName", _Student.LastName );
                    paramater.Add( "@DateOfBirth", _Student.DateOfBirth );
                    paramater.Add( "@GenderID", _Student.GenderID );
                    paramater.Add( "@SchoolID", _Student.SchoolID );
                    paramater.Add( "@GradeID", _Student.GradeID );
                    paramater.Add( "@Active", _Student.Active );
                    paramater.Add( "@CreateUserID", _Student.CreateUserID );
                    var value = con.Query<int>( "sp_save_Student", paramater, null, true, 0, commandType: CommandType.StoredProcedure );
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "InsertStudent", _Ex.Message );
            }
        }

        public bool CheckStudentExits( string _sFirstName, string _sLastName,
            int _iSchoolId, int _iGradeId, int _iCurId = 0 )
        {
            try
            {
                var _result = ( from _Student in _context.StudentMaster
                                where ( _Student.StudentID != _iCurId
                                && _Student.FirstName == _sFirstName
                                && _Student.LastName == _sLastName
                                && _Student.SchoolID == _iSchoolId
                                && _Student.GradeID == _iGradeId )
                                select _Student ).Count();

                return _result > 0 ? true : false;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "CheckStudentExits", _Ex.Message );
            }
            return false;
        }

        public List<StudentMasterDisplayViewModel> GetStudentMasterList()
        {
            try
            {
                var result = ( from _Student in _context.StudentMaster
                               join _Gender in _context.GenderLookup on _Student.GenderID equals _Gender.GenderID
                               join _School in _context.SchoolMaster on _Student.SchoolID equals _School.SchoolID
                               join _Grade in _context.SchoolGradeLookup on _Student.GradeID equals _Grade.GradeID
                               select new StudentMasterDisplayViewModel
                               {
                                   StudentID = _Student.StudentID,
                                   FirstName = _Student.FirstName,
                                   OtherName = _Student.OtherName,
                                   LastName = _Student.LastName,
                                   FullName = GetFullName( _Student.FirstName, _Student.OtherName, _Student.LastName ),
                                   GenderID = _Student.GenderID,
                                   Gender = (_Gender.GenderName == null ? "Not Given": _Gender.GenderName ),
                                   DateOfBirth = _Student.DateOfBirth,
                                   SchoolID = _Student.SchoolID,
                                   SchoolName = _School.SchoolName,
                                   SchoolCode = _School.SchoolCode,
                                   GradeID = _Student.GradeID,
                                   Active = _Student.Active,
                                   SchoolGrade = _Grade.GradeName
                               } ).OrderBy( _x => _x.SchoolName ).ThenBy( _y => _y.LastName ).ToList();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetStudentMasterList", _Ex.Message );
            }
            return null;
        }

        private string GetFullName( string _sFirstName, string _sOtherName, string _sLastName )
        {
            try
            {
                if ( _sOtherName == null || _sOtherName.Trim() == String.Empty )
                {
                    return ( _sLastName.Trim() + " " + _sFirstName.Trim() );
                }
                return ( _sLastName.Trim() + " " + _sFirstName.Trim() + " " + _sOtherName.Trim() );

            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetFullName", _Ex.Message );
            }
            return string.Empty;
        }

        public bool UpdateStudentMaster( StudentMaster StudentMaster )
        {
            try
            {
                _context.Entry( StudentMaster ).Property( x => x.FirstName ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.OtherName ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.LastName ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.GenderID ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.DateOfBirth ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.SchoolID ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.GradeID ).IsModified = true;
                _context.Entry( StudentMaster ).Property( x => x.CreateUserID ).IsModified = true;
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "UpdateStudentMaster", _Ex.Message );
            }
            return false;
        }

        public bool DeleteStudent( int StudentId )
        {
            try
            {
                var StudentMaster = ( from Student in _context.StudentMaster
                                      where Student.StudentID == StudentId
                                      select Student ).FirstOrDefault();
                if ( StudentMaster != null )
                {
                    _context.StudentMaster.Remove( StudentMaster );
                    return ( _context.SaveChanges() > 0 );
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "DeleteStudent", _Ex.Message );
            }
            return false;
        }

        public StudentMaster GetStudentMasterById( int _iStudentId )
        {
            try
            {
                var result = ( from _Student in _context.StudentMaster
                               where _Student.StudentID == _iStudentId
                               select _Student ).FirstOrDefault();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetStudentMasterbyId", _Ex.Message );
            }
            return null;
        }
    }
}
