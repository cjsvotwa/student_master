﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Concrete
{
    public class UserConcrete : IUser
    {

        private readonly DatabaseContext _context;
        public UserConcrete( DatabaseContext context )
        {
            _context = context;
        }

        public bool CheckUserExits( string username )
        {
            try
            {
                return ( ( from _User in _context.Users
                           where _User.UserName == username
                           select _User ).Count() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "CheckUserExits", _Ex.Message );
            }
            return false;
        }

        public bool AuthenticateUser( string _sUsername, string _sPassword )
        {
            try
            {
                return ( ( from _User in _context.Users
                           where _User.UserName == _sUsername && _User.Password == _sPassword
                           select _User ).Count() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "AuthenticateUser", _Ex.Message );
            }
            return false;
        }

        public LoginResponse GetUserDetailsByCredentials( string _sUsername )
        {
            try
            {
                var _result = ( from user in _context.Users
                               where user.UserName == _sUsername
                               select new LoginResponse
                               {
                                   UserId = user.UserID,
                                   RoleId = 1,
                                   Status = user.Active,
                                   UserName = user.UserName
                               } ).SingleOrDefault();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetUserDetailsByCredentials", _Ex.Message );
            }
            return null;
        }

        public List<User> GetUserList()
        {
            try
            {
                var _result = ( from user in _context.Users
                               where user.Active == true
                               select user ).ToList();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetUserList", _Ex.Message );
            }
            return null;
        }

        public User GetUserById( int _iUserId )
        {
            try
            {
                var _result = ( from user in _context.Users
                               where user.UserID == _iUserId
                               select user ).FirstOrDefault();
                return _result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetUserById", _Ex.Message );
            }
            return null;
        }
    }
}
