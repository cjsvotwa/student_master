﻿using System.Collections.Generic;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Interface
{
    public interface IGenderLookup
    {
        List<GenderLookup> GetGenderList();
        GenderLookup GetGenderById( int GenderId );
    }
}
