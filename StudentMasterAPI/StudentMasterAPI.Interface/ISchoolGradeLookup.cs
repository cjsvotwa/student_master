﻿using System.Collections.Generic;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Interface
{
    public interface ISchoolGradeLookup
    {
        List<SchoolGradeLookup> GetSchoolGradeList();
        SchoolGradeLookup GetGradeById( int _iGradeId );
    }
}
