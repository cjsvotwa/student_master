﻿using System.Collections.Generic;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Interface
{
    public interface ISchoolMaster
    {
        bool AddSchoolMaster( SchoolMaster SchoolMaster );
        List<SchoolMaster> GetSchoolMasterList();
        SchoolMaster GetSchoolMasterbyId( int SchoolId );
        bool CheckSchoolNameExists( string SchoolName, int id = 0 );
        bool UpdateSchoolMaster( SchoolMaster SchoolMaster );
        bool DeleteSchool( int SchoolId );
        List<SchoolMaster> GetActiveSchoolMasterList();
    }
}
