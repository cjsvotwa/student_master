﻿using System.Collections.Generic;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Interface
{
    public interface IStudentMaster
    {
        void InsertStudent( StudentMaster Student );
        bool CheckStudentExits( string _sFirstName, string _sLastName,
            int _iSchoolId, int _iGradeId, int _iCurId = 0 );
        List<StudentMasterDisplayViewModel> GetStudentMasterList();
        StudentMaster GetStudentMasterById( int _iStudentId );
        bool DeleteStudent( int StudentId );
        bool UpdateStudentMaster( StudentMaster StudentMaster );
    }
}