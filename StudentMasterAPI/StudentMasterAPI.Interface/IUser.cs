﻿using System.Collections.Generic;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Interface
{
    public interface IUser
    {
        bool CheckUserExits( string _sUsername );
        User GetUserById( int _iUserid );
        List<User> GetUserList();
        bool AuthenticateUser( string _sUsername, string _sPassword );
        LoginResponse GetUserDetailsByCredentials( string _sUsername );
    }
}