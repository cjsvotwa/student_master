﻿using System.ComponentModel.DataAnnotations;

namespace StudentMasterAPI.Models
{
    public class GenderLookup
    {
        [Key]
        public int GenderID { get; set; }
        public string GenderName { get; set; }
    }
}
