﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentMasterAPI.Models
{
    public class SchoolGradeLookup
    {
        [Key]
        public int GradeID { get; set; }
        public int SequenceNo { get; set; }
        public string GradeName { get; set; }
    }
}
