﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentMasterAPI.Models
{
    public class SchoolMaster
    {
        [Key]
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string SchoolCode { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Active { get; set; }      
    }
}
