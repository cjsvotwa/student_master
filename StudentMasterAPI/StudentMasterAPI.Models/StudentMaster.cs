﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentMasterAPI.Models
{
    public class StudentMaster
    {
        [Key]
        public int StudentID { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int GenderID { get; set; }
        public int SchoolID { get; set; }
        public int GradeID { get; set; }
        public bool Active { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateUserID { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? ModifyUserID { get; set; }
    }

}
