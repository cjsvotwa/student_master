﻿using System.ComponentModel.DataAnnotations;

namespace StudentMasterAPI.ViewModels
{
    public class SchoolMasterViewModel
    {
        [Required( ErrorMessage = "School Name is Required" )]
        public string SchoolName { get; set; }

        [Required( ErrorMessage = "School Code is Required" )]
        public string SchoolCode { get; set; }

        public bool Active { get; set; }
    }
    public class SchoolMasterEditViewModel
    {
        [Required( ErrorMessage = "School ID is Required" )]
        public int SchoolID { get; set; }

        [Required( ErrorMessage = "School Name is Required" )]
        public string SchoolName { get; set; }

        [Required( ErrorMessage = "School Code is Required" )]
        public string SchoolCode { get; set; }
        public bool Active { get; set; }
    }


}
