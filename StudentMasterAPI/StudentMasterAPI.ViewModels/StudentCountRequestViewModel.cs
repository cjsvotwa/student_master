﻿using System.ComponentModel.DataAnnotations;

namespace StudentMasterAPI.ViewModels
{
    public class StudentCountRequestViewModel
    {
        [Required(ErrorMessage = "School is Required")]
        public int SchoolId { get; set; }
    }
}
