﻿using System;

namespace StudentMasterAPI.ViewModels
{
    public class StudentMasterViewModel
    {
        public int StudentID { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int GenderID { get; set; }
        public string Gender { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string SchoolCode { get; set; }
        public int GradeID { get; set; }
        public int SchoolGrade { get; set; }
        public bool Active { get; set; }
    }

    public class StudentMasterDisplayViewModel
    {
        public int StudentID { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int GenderID { get; set; }
        public string Gender { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string SchoolCode { get; set; }
        public int GradeID { get; set; }
        public string SchoolGrade { get; set; }
        public bool Active { get; set; }
    }

    public class ActiveStudentModel
    {
        public string StudentID { get; set; }
        public string StudentName { get; set; }
    }
}
