﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Common;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class GenderLookupController : ControllerBase
    {
        private readonly IGenderLookup _GenderLookupController;
        public GenderLookupController( IGenderLookup _GenderLookupController )
        {
            this._GenderLookupController = _GenderLookupController;
        }
        // GET: api/GenderLookup
        [HttpGet]
        public IEnumerable<GenderLookup> Get()
        {
            try
            {
                return _GenderLookupController.GetGenderList();
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Get", _Ex.Message );
            }
            return null;
        }


    }
}
