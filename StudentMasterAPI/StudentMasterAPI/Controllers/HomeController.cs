﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Controllers
{
    public class HomeController  : Controller
    {
        private ISchoolMaster _SchoolMaster;
        public HomeController(ISchoolMaster SchoolMaster)
        {
            _SchoolMaster = SchoolMaster;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Contact page.";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
