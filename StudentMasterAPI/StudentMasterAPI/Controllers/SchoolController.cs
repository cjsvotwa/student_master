﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Common;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class SchoolController : ControllerBase
    {
        private readonly ISchoolMaster _SchoolMaster;
        public SchoolController( ISchoolMaster SchoolMaster )
        {
            _SchoolMaster = SchoolMaster;
        }

        // GET: api/School
        [HttpGet]
        public List<SchoolMaster> Get()
        {
            return _SchoolMaster.GetSchoolMasterList();
        }

        // GET: api/School/5
        [HttpGet( "{id}", Name = "GetSchool" )]
        public SchoolMaster Get( int id )
        {
            return _SchoolMaster.GetSchoolMasterbyId( id );
        }

        // POST: api/School
        [HttpPost]
        public HttpResponseMessage Post( [FromBody] SchoolMasterViewModel SchoolMaster )
        {
            try
            {
                if ( ModelState.IsValid )
                {
                    if ( _SchoolMaster.CheckSchoolNameExists( SchoolMaster.SchoolCode ) )
                    {
                        var response = new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.Conflict
                        };
                        return response;
                    }
                    else
                    {
                        var userId = this.User.FindFirstValue( ClaimTypes.Name );
                        var tempSchoolMaster = AutoMapper.Mapper.Map<SchoolMaster>( SchoolMaster );
                        tempSchoolMaster.CreatedDate = DateTime.Now;
                        tempSchoolMaster.CreatedBy = Convert.ToInt32( userId );
                        _SchoolMaster.AddSchoolMaster( tempSchoolMaster );
                        var response = new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.OK
                        };
                        return response;
                    }
                }
                else
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest
                    };
                    return response;
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Post", _Ex.Message );
            }
            return null;
        }

        // PUT: api/School/5
        [HttpPut( "{id}" )]
        public HttpResponseMessage Put( int id, [FromBody] SchoolMasterEditViewModel SchoolMaster )
        {
            try
            {
                if ( string.IsNullOrWhiteSpace( Convert.ToString( id ) ) || SchoolMaster == null )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest
                    };
                    return response;
                }
                if ( _SchoolMaster.CheckSchoolNameExists( SchoolMaster.SchoolCode, SchoolMaster.SchoolID ) )
                {
                    var _response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Conflict
                    };
                    return _response;
                }
                var temp = AutoMapper.Mapper.Map<SchoolMaster>( SchoolMaster );
                var result = _SchoolMaster.UpdateSchoolMaster( temp );
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK
                };
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Put", _Ex.Message );
            }
            return null;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete( "{id}" )]
        public HttpResponseMessage Delete( int id )
        {
            try
            {
                var result = _SchoolMaster.DeleteSchool( id );
                if ( result )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK
                    };
                    return response;
                }
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest
                };
                return _response;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Delete", _Ex.Message );
            }
            return null;
        }
    }
}
