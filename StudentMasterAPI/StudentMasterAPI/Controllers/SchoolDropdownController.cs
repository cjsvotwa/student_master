﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Common;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class SchoolDropdownController : ControllerBase
    {
        private readonly ISchoolMaster _SchoolMaster;
        public SchoolDropdownController( ISchoolMaster SchoolMaster )
        {
            _SchoolMaster = SchoolMaster;
        }
        // GET: api/SchoolDropdown
        [HttpGet]
        public IEnumerable<SchoolMaster> Get()
        {
            try
            {
                return _SchoolMaster.GetActiveSchoolMasterList();
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Get", _Ex.Message );
            }
            return null;
        }


    }
}
