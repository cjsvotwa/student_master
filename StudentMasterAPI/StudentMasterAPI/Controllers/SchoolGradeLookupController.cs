﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Common;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class SchoolGradeLookupController : ControllerBase
    {
        private readonly ISchoolGradeLookup _SchoolGradeLookupController;
        public SchoolGradeLookupController( ISchoolGradeLookup _SchoolGradeLookupController )
        {
            this._SchoolGradeLookupController = _SchoolGradeLookupController;
        }
        // GET: api/SchoolGradeLookup
        [HttpGet]
        public IEnumerable<SchoolGradeLookup> Get()
        {
            try
            {
                return _SchoolGradeLookupController.GetSchoolGradeList();
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Get", _Ex.Message );
            }
            return null;
        }


    }
}
