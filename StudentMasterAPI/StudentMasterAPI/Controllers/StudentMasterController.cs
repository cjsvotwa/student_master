﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Common;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class StudentMasterController : ControllerBase
    {
        private readonly IStudentMaster _StudentMaster;
        public StudentMasterController( IStudentMaster StudentMaster )
        {
            _StudentMaster = StudentMaster;
        }
        // GET: api/StudentMaster
        [HttpGet]
        public IEnumerable<StudentMasterDisplayViewModel> Get()
        {
            return _StudentMaster.GetStudentMasterList();
        }

        // GET: api/StudentMaster/5
        [HttpGet( "{id}", Name = "GetStudent" )]
        public StudentMaster Get( int id )
        {
            try
            {
                return _StudentMaster.GetStudentMasterById( id );
            }
            catch ( Exception )
            {
                throw;
            }
        }

        // POST: api/StudentMaster
        [HttpPost]
        public HttpResponseMessage Post( [FromBody] StudentMasterViewModel _viewModel )
        {
            try
            {

                if ( _StudentMaster.CheckStudentExits( _viewModel.FirstName, _viewModel.LastName,
             _viewModel.SchoolID, _viewModel.GradeID, _viewModel.StudentID ) )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Conflict
                    };
                    return response;
                }
                else
                {
                    var userId = this.User.FindFirstValue( ClaimTypes.Name );
                    var tempStudentMaster = AutoMapper.Mapper.Map<StudentMaster>( _viewModel );
                    tempStudentMaster.CreateUserID = Convert.ToInt32( userId );
                    _StudentMaster.InsertStudent( tempStudentMaster );

                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK
                    };
                    return response;
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Post", _Ex.Message );
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return response;
            }
        }

        // PUT: api/StudentMaster/5
        [HttpPut( "{id}" )]
        public HttpResponseMessage Put( int id, [FromBody] StudentMasterViewModel _viewModel )
        {
            try
            {
                if ( _StudentMaster.CheckStudentExits( _viewModel.FirstName, _viewModel.LastName,
                    _viewModel.SchoolID, _viewModel.GradeID, _viewModel.StudentID ) )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Conflict
                    };
                    return response;
                }
                var userId = this.User.FindFirstValue( ClaimTypes.Name );
                var tempStudentMaster = AutoMapper.Mapper.Map<StudentMaster>( _viewModel );
                tempStudentMaster.CreateUserID = Convert.ToInt32( userId );
                _StudentMaster.UpdateStudentMaster( tempStudentMaster );
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK
                };

                return _response;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Post", _Ex.Message );
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return _response;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete( "{id}" )]
        public HttpResponseMessage Delete( int id )
        {
            try
            {
                _StudentMaster.DeleteStudent( id );
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK
                };
                return _response;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Delete", _Ex.Message );
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return response;
            }
        }
    }
}
