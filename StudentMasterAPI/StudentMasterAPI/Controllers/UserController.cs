﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Models;

namespace StudentMasterAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUser _users;
        public UserController(IUser users)
        {
            _users = users;
        }
        // GET: api/User
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return _users.GetUserList();
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "GetUsers")]
        public User Get(int id)
        {
            return _users.GetUserById(id);
        }
    }
}
