﻿using AutoMapper;
using StudentMasterAPI.Models;
using StudentMasterAPI.ViewModels;

namespace StudentMasterAPI.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SchoolMasterViewModel, SchoolMaster>()
                .ForMember( dest => dest.SchoolName, opt => opt.MapFrom( src => src.SchoolName ) )
                .ForMember( dest => dest.Active, opt => opt.MapFrom( src => src.Active ) );

            CreateMap<SchoolMasterEditViewModel, SchoolMaster>()
                .ForMember( dest => dest.SchoolName, opt => opt.MapFrom( src => src.SchoolName ) )
                .ForMember( dest => dest.SchoolCode, opt => opt.MapFrom( src => src.SchoolCode ) )
                .ForMember( dest => dest.SchoolID, opt => opt.MapFrom( src => src.SchoolID ) )
                .ForMember( dest => dest.Active, opt => opt.MapFrom( src => src.Active ) );

            CreateMap<StudentMasterViewModel, StudentMaster>()
                .ForMember( dest => dest.StudentID, opt => opt.MapFrom( src => src.StudentID ) )
                .ForMember( dest => dest.FirstName, opt => opt.MapFrom( src => src.FirstName ) )
                .ForMember( dest => dest.OtherName, opt => opt.MapFrom( src => src.OtherName ) )
                .ForMember( dest => dest.LastName, opt => opt.MapFrom( src => src.LastName ) )
                .ForMember( dest => dest.DateOfBirth, opt => opt.MapFrom( src => src.DateOfBirth ) )
                .ForMember( dest => dest.GenderID, opt => opt.MapFrom( src => src.GenderID ) )
                .ForMember( dest => dest.SchoolID, opt => opt.MapFrom( src => src.SchoolID ) )
                .ForMember( dest => dest.GradeID, opt => opt.MapFrom( src => src.GradeID ) )
                .ForMember( dest => dest.Active, opt => opt.MapFrom( src => src.Active ) );
        }
    }
}
