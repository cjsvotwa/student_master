﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using StudentMasterAPI.Common;
using StudentMasterAPI.Concrete;
using StudentMasterAPI.Interface;
using StudentMasterAPI.Mappings;
using StudentMasterAPI.Models;

namespace StudentMasterAPI
{
    public class Startup
    {
        public Startup( IConfiguration configuration )
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services )
        {
            services.Configure<CookiePolicyOptions>( options =>
             {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                 options.MinimumSameSitePolicy = SameSiteMode.None;
             } );

            #region MyRegion
            var connection = Configuration.GetConnectionString( "DatabaseConnection" );
            services.AddDbContext<DatabaseContext>( options => options.UseSqlServer( connection, b => b.UseRowNumberForPaging() ) );

            var appSettingsSection = Configuration.GetSection( "AppSettings" );
            services.Configure<AppSettings>( appSettingsSection );

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes( appSettings.Secret );
            services.AddAuthentication( x =>
                 {
                     x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                     x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                 } )
                .AddJwtBearer( x =>
                 {
                     x.RequireHttpsMetadata = false;
                     x.SaveToken = true;
                     x.TokenValidationParameters = new TokenValidationParameters
                     {
                         ValidateIssuerSigningKey = true,
                         IssuerSigningKey = new SymmetricSecurityKey( key ),
                         ValidateIssuer = false,
                         ValidateAudience = false
                     };
                 } );
            services.AddSingleton<IConfiguration>( Configuration );
            services.AddTransient<ISchoolMaster, SchoolMasterConcrete>();
            services.AddTransient<IStudentMaster, StudentMasterConcrete>();
            services.AddTransient<IGenderLookup, GenderLookupConcrete>();
            services.AddTransient<ISchoolGradeLookup, SchoolGradeLookupConcrete>();
            services.AddTransient<IUser, UserConcrete>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper>( implementationFactory =>
             {
                 var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                 return new UrlHelper( actionContext );
             } );
            #endregion

            // Start Registering and Initializing AutoMapper

            Mapper.Initialize( cfg => cfg.AddProfile<MappingProfile>() );
            services.AddAutoMapper();

            // End Registering and Initializing AutoMapper

            services.AddMvc( options => { options.Filters.Add( typeof( CustomExceptionFilterAttribute ) ); } )
            .SetCompatibilityVersion( CompatibilityVersion.Version_2_1 )
            .AddJsonOptions( options =>
             {
                 options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
             } );
            services.AddCors( options =>
             {
                 options.AddPolicy( "CorsPolicy",
                     builder => builder.AllowAnyOrigin()
                         .AllowAnyMethod()
                         .AllowAnyHeader()
                         .AllowCredentials()
                         .WithExposedHeaders( "X-Pagination" ) );
             } );

            services.AddSwaggerDocumentation();
            #region OLD Working code for swagger configuration
            //// Register the Swagger generator, defining 1 or more Swagger documents
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info { Title = "WebGym API", Version = "v1" });
            //}); 
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IHostingEnvironment env )
        {
            if ( env.IsDevelopment() )
            {
                app.UseDeveloperExceptionPage();
                //Configure Swagger only for development purose not for production app.
                app.UseSwaggerDocumentation();
            }
            else
            {
                app.UseExceptionHandler( "/Home/Error" );
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseCors( "CorsPolicy" );
            app.UseMvc( routes =>
             {
                 routes.MapRoute(
                     name: "default",
                     template: "{controller=Home}/{action=Index}/{id?}" );
             } );
        }


    }
    public static class SwaggerServiceExtensions
    {
        public static IServiceCollection AddSwaggerDocumentation( this IServiceCollection services )
        {
            services.AddSwaggerGen( c =>
             {
                 c.SwaggerDoc( "v1.0", new Info { Title = "Main API v1.0", Version = "v1.0" } );

                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                 var xmlPath = Path.Combine( AppContext.BaseDirectory, xmlFile );
                 if ( File.Exists( xmlPath ) )
                 {
                    //... and tell Swagger to use those XML comments.
                    c.IncludeXmlComments( xmlPath );
                 }

                // Swagger 2.+ support
                var security = new Dictionary<string, IEnumerable<string>>
                 {
                    {"Bearer", new string[] { }},
                 };
                 c.AddSecurityDefinition( "Bearer", new ApiKeyScheme
                 {
                     Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                     Name = "Authorization",
                     In = "header",
                     Type = "apiKey"
                 } );
                //Must require for swagger version > 2.0
                c.AddSecurityRequirement( security );
             } );

            return services;
        }

        public static IApplicationBuilder UseSwaggerDocumentation( this IApplicationBuilder app )
        {
            app.UseSwagger();
            app.UseSwaggerUI( c =>
             {
                 c.SwaggerEndpoint( "/swagger/v1.0/swagger.json", "Student Master Web API" );
                 c.DocumentTitle = "API in Summary";
                 c.RoutePrefix = string.Empty;
             } );

            return app;
        }
    }
}
