export class SchoolMasterViewModel 
{
    public SchoolID: number = 0;
    public SchoolName: string = "";
    public SchoolCode: string = "";
    public Active: boolean = false;
    public CreatedDate : string = ""
}
