import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { SchoolMasterModel } from "../app.SchoolModel";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { SchoolMasterViewModel } from "../Models/app.SchoolViewModel";
import { SchoolDropdownModel } from "../Models/app.SchoolDropdownModel";
// import { environment } from "src/app/Shared/environment";
import{environment} from '../../../environments/environment';
import { SchoolGradeLookupModel } from "../Models/app.SchoolGradeLookupModel";

@Injectable({
    providedIn: 'root'
})

export class SchoolService {
    private data: any;
    private apiUrl = environment.apiEndpoint + "/api/School/";
    token: any;
    username: any;

    constructor(private http: HttpClient) {
        this.data = JSON.parse(localStorage.getItem('AdminUser'));
        this.token = this.data.token;
        this.username = this.data.username
    }

    // Save School
  public SaveSchool(schoolMasterModel: SchoolMasterModel) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.post<any>(this.apiUrl, schoolMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );

    }

    // Get All School
  public GetAllSchool() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<SchoolMasterViewModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Get All School List
  public GetAllActiveSchoolList() {
    var apiUrl = environment.apiEndpoint + "/api/SchoolDropdown/";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<SchoolDropdownModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
  }

  // Get Grade Lookup data
  public GetGradeLookup() {
    var apiUrl = environment.apiEndpoint + "/api/SchoolGradeLookup";
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<SchoolGradeLookupModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
      catchError(this.handleError)
    );
  }

    // Get School By ID
    public GetSchoolById(schoolId) {
      var editUrl = this.apiUrl + '/' + schoolId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SchoolMasterViewModel>(editUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Update School
    public UpdateSchool(schoolMasterModel: SchoolMasterModel) {
      var putUrl = this.apiUrl + '/' + schoolMasterModel.SchoolID;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this.http.put<any>(putUrl, schoolMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    public DeleteSchool(schoolId) {
        var deleteUrl = this.apiUrl + '/' + schoolId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.delete<any>(deleteUrl, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };



}
