import { Component, OnInit, ViewChild } from '@angular/core';
import { SchoolService } from './Services/app.School.Service';
import { SchoolMasterViewModel } from './Models/app.SchoolViewModel';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './app.AllSchoolComponent.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css', '../Content/vendor/font-awesome/css/font-awesome.min.css']

})

export class AllSchoolComponent implements OnInit {
  private _SchoolService;
  AllSchoolList: SchoolMasterViewModel[];
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['SchoolID', 'SchoolName', 'SchoolCode', 'Active', 'EditAction', 'DeleteAction'];
  dataSource: any;
  
  


  constructor(private location: Location, private _Route: Router, private schoolService: SchoolService) {
    this._SchoolService = schoolService;

  
  }

  ngOnInit() {


    this._SchoolService.GetAllSchool().subscribe(
      AllSchool => {
        this.AllSchoolList = AllSchool
        this.dataSource = new MatTableDataSource(this.AllSchoolList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
       
      },
      error => this.errorMessage = <any>error
    );

  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Delete(schoolId): void {
    if (confirm("Are you sure to delete School ?")) {
      this._SchoolService.DeleteSchool(schoolId).subscribe
        (
        response => {
          if (response.StatusCode == "200") {
            alert('Deleted School Successfully');
            location.reload();
          }
          else {
            alert('Something Went Wrong');
            this._Route.navigate(['/School/All']);
          }
        }
        )
    }
  }

}
