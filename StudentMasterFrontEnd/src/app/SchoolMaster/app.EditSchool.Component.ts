import { Component, OnInit } from '@angular/core';
import { SchoolService } from './Services/app.School.Service';
import { SchoolMasterModel } from './app.SchoolModel';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBarConfig } from '@angular/material';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBar } from '@angular/material';

@Component({
  templateUrl: './app.EditSchoolComponent.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class EditSchoolComponent implements OnInit {
  title = "Edit School Master";
  SchoolForms: SchoolMasterModel = new SchoolMasterModel();
  private _SchoolService;
  private responsedata: any;
  private SchoolID: string;
  errorMessage: any;

  actionButtonLabel: string = 'Retry';
  action: boolean = false;
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';

  constructor(private _Route: Router, private _routeParams: ActivatedRoute, public snackBar: MatSnackBar, private schoolService: SchoolService) {
    this._SchoolService = schoolService;
  }

  ngOnInit() {
    this.SchoolID = this._routeParams.snapshot.params['schoolId'];
    if (this.SchoolID != null) {
      var data = this._SchoolService.GetSchoolById(this.SchoolID).subscribe(
        School => {
          this.SchoolForms.SchoolID = School.SchoolID;
          this.SchoolForms.SchoolName = School.SchoolName;
          this.SchoolForms.SchoolCode = School.SchoolCode;
          this.SchoolForms.Active = School.Active;
        },
        error => this.errorMessage = <any>error
      );
    }
  }


  onSubmit() {


    this._SchoolService.UpdateSchool(this.SchoolForms)
      .subscribe(response => {
        if (response.StatusCode == "200") {
          alert('Updated School Successfully');
          this._Route.navigate(['/School/All']);
        } else if (response.StatusCode == "409") {
          let config = new MatSnackBarConfig();
          config.duration = this.setAutoHide ? this.autoHide : 0;
          config.verticalPosition = this.verticalPosition;
          this.snackBar.open("School Code Already Exists", this.action ? this.actionButtonLabel : undefined, config);
        }
      })
  }
}
