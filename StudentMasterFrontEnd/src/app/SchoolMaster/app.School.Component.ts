import { Component } from '@angular/core';
import { SchoolMasterModel } from './app.SchoolModel';
import { SchoolService } from './Services/app.School.Service';
import { Router } from '@angular/router';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
    templateUrl: './app.SchoolMaster.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class SchoolComponent {
    title = "School Master";
    SchoolForms: SchoolMasterModel = new SchoolMasterModel();
    private _SchoolService;
    private responsedata: any;
    
    actionButtonLabel: string = 'Retry';
    action: boolean = false;
    setAutoHide: boolean = true;
    autoHide: number = 2000;
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';



    constructor(private _Route: Router, public snackBar: MatSnackBar,  private schoolService: SchoolService) {
        this._SchoolService = schoolService;
    }
    output: any;
    onSubmit() {
      

        this._SchoolService.SaveSchool(this.SchoolForms).subscribe(
            response => 
            {
               
                this.output = response;
                if (this.output.StatusCode == "409") 
                {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("School Name Already Exists", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
                else if (this.output.StatusCode == "200") 
                { 
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Saved School Successfully", this.action ? this.actionButtonLabel : undefined, config);
                    this._Route.navigate(['/School/All']);
                }
                else {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Something Went Wrong", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
            }
        );



    }

}
