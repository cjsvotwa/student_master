export class StudentMasterModel {
  public StudentID: number;
  public FirstName: string;
  public OtherName: string;
  public LastName: string;
  public FullName: string;
  public DateOfBirth: Date;
  public GenderID: number = 0;
  public SchoolID: number = 0;
  public GradeID: number = 0;
  public Active: boolean = false;
}

