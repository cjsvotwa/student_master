export class StudentMasterViewModel {
  public StudentID: number;
  public FirstName: string;
  public OtherName: string;
  public LastName: string;
  public FullName: string;
  public DateOfBirth: Date;
  public Gender: string;
  public SchoolCode: string;
  public SchoolName: string;
  public SchoolGrade: string;
}
