import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { StudentMasterModel } from '../Models/app.StudentMasterModel';
import { StudentMasterViewModel } from '../Models/app.StudentMasterViewModel';
import { ActiveStudentModel } from '../Models/app.ActiveStudentModel';
// import { environment } from 'src/app/Shared/environment';
import { environment } from '../../../environments/environment';
import { GenderLookupModel } from '../Models/app.GenderLookupModel';


@Injectable({
  providedIn: 'root'
})

export class StudentService {
  private data: any;
  private apiUrl = environment.apiEndpoint + "/api/StudentMaster/";
  token: any;
  username: any;


  constructor(private http: HttpClient) {
    this.data = JSON.parse(localStorage.getItem('AdminUser'));
    this.token = this.data.token;
  }

  // Save Student
  public SaveStudent(studentMasterModel: StudentMasterModel) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.post<any>(this.apiUrl, studentMasterModel, { headers: headers })
      .pipe(
        catchError(this.handleError)
      );

  }

  // Get All Students
  public GetAllStudents() {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<StudentMasterViewModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
      catchError(this.handleError)
    );
  }

  // Get Gender Lookup data
  public GetGenderLookup() {
    var apiUrl = environment.apiEndpoint + "/api/GenderLookup";
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<GenderLookupModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
      catchError(this.handleError)
    );
  }

  // Get Student by StudentId
  public GetStudentByStudentID(studentId) {
    var editUrl = this.apiUrl + '/' + studentId;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<StudentMasterModel>(editUrl, { headers: headers }).pipe(tap(data => data),
      catchError(this.handleError)
    );
  }

  // Update Student
  public UpdateStudent(studentMasterModel: StudentMasterModel) {
    var updateurl = this.apiUrl + studentMasterModel.StudentID;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.put<any>(updateurl, studentMasterModel, { headers: headers })
      .pipe(
        catchError(this.handleError)
      );
  }

  public DeleteStudent(studentId) {
    var deleteUrl = this.apiUrl + '/' + studentId;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.delete<any>(deleteUrl, { headers: headers })
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

}
