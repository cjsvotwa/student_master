import { Component, OnInit } from '@angular/core';
import { SchoolService } from '../SchoolMaster/Services/app.School.Service';
import { SchoolDropdownModel } from '../SchoolMaster/Models/app.SchoolDropdownModel';
import { StudentService } from '../StudentMaster/Services/app.studentmaster.service';
import { StudentMasterModel } from './Models/app.StudentMasterModel';
import { Router, ActivatedRoute } from '@angular/router'
import { SchoolGradeLookupModel } from '../SchoolMaster/Models/app.SchoolGradeLookupModel';
import { GenderLookupModel } from './Models/app.GenderLookupModel';

@Component({
    templateUrl: './app.EditStudent.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css'
    ]
})


export class EditStudentComponent implements OnInit {
    private _schoolService;
    private _studentService;
  AllActiveSchoolList: SchoolDropdownModel[];
  AllSchoolGradeList: SchoolGradeLookupModel[];
  AllGenderList: GenderLookupModel[];
    errorMessage: any;
    studentModel: StudentMasterModel = new StudentMasterModel();
    StudentID: any;
    output: any;


    constructor(private _Route: Router, private _routeParams: ActivatedRoute, private schoolService: SchoolService, private studentService: StudentService) {
        this._schoolService = schoolService;
        this._studentService = studentService;
    }

    ngOnInit(): void {

      this.StudentID = this._routeParams.snapshot.params['StudentID'];
       
        this._schoolService.GetAllActiveSchoolList().subscribe(
            allActiveSchool => {
                this.AllActiveSchoolList = allActiveSchool
            },
            error => this.errorMessage = <any>error
        );
      this._schoolService.GetGradeLookup().subscribe(
        allSchoolGrade => {
          this.AllSchoolGradeList = allSchoolGrade
        },
        error => this.errorMessage = <any>error
      );
      this._studentService.GetGenderLookup().subscribe(
        allGenderList => {
          this.AllGenderList = allGenderList
        },
            error => this.errorMessage = <any>error
        );

        // GetStudentByStudentID

        this._studentService.GetStudentByStudentID(this.StudentID).subscribe(
            student => {
                this.studentModel = student
            },
            error => this.errorMessage = <any>error
        );
    }


    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }


    onSubmit() 
    {
        this._studentService.UpdateStudent(this.studentModel).subscribe(
            response => {
                this.output = response
                if (this.output.StatusCode == "409") {
                    alert('Student Already Exists');
                }
                else if (this.output.StatusCode == "200") {
                    alert('Student Saved Successfully');
                    this._Route.navigate(['/Student/All']);
                }
                else {
                    alert('Something Went Wrong');
                }
            });
    }
}
