import { Component, OnInit, ViewChild } from '@angular/core';
import { StudentService } from './Services/app.studentmaster.service';
import { StudentMasterViewModel } from './Models/app.StudentMasterViewModel';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './app.allstudentmaster.component.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
    '../Content/vendor/metisMenu/metisMenu.min.css',
    '../Content/dist/css/sb-admin-2.css',
    '../Content/vendor/font-awesome/css/font-awesome.min.css'
  ]
})

export class AllStudentMasterComponent implements OnInit {
  private _studentService;
  StudentList: StudentMasterViewModel = new StudentMasterViewModel();
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['StudentID', 'FullName', 'DateOfBirth', 'Gender', 'SchoolGrade', 'SchoolCode', 'SchoolName', 'EditAction', 'DeleteAction'];
  dataSource: any;
  constructor(private _Route: Router, private studentService: StudentService) {
    this._studentService = studentService;
  }
  ngOnInit(): void {


    this._studentService.GetAllStudents().subscribe(
      allstudent => {
        this.StudentList = allstudent
        this.dataSource = new MatTableDataSource(allstudent);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error => this.errorMessage = <any>error
    );
  }


  Delete(StudentID) {
    if (confirm("Are you sure to delete Student ?")) {
      this._studentService.DeleteStudent(StudentID).subscribe
        (
        response => {
          if (response.StatusCode == "200") {
            alert('Deleted Student Successfully');
            location.reload();
          }
          else {
            alert('Something Went Wrong');
            this._Route.navigate(['/Student/All']);
          }
        }
        )
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
