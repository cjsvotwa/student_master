import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule, } from 'ngx-bootstrap/datepicker';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatAutocomplete, MatAutocompleteModule } from '@angular/material/autocomplete';
import { AppComponent } from './app.component';
import { SchoolComponent } from './SchoolMaster/app.School.Component';
import { AllSchoolComponent } from './SchoolMaster/app.AllSchool.Component';
import { EditSchoolComponent } from './SchoolMaster/app.EditSchool.Component';
import { StudentMasterComponent } from './StudentMaster/app.studentmaster.component';
import { AllStudentMasterComponent } from './StudentMaster/app.allstudentmaster.component';
import { EditStudentComponent } from './StudentMaster/app.EditStudent.component';
import { MatSortModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, MatSnackBar, MatSnackBarConfig, MatSnackBarModule } from '@angular/material';
import { LoginComponent } from './Login/app.LoginComponent';
import { AppAdminLayoutComponent } from './_layout/app-adminlayout.component';
import { AdminDashboardComponent } from './AdminDashboard/app.AdminDashboardComponent';
import { AdminLogoutComponent } from './Login/app.AdminLogout.Component';
import { AdminAuthGuardService } from './AuthGuard/AdminAuthGuardService';


@NgModule({
  declarations: [
    AppComponent,
    AppAdminLayoutComponent,
    SchoolComponent,
    AllSchoolComponent,
    EditSchoolComponent,
    StudentMasterComponent,
    AllStudentMasterComponent,
    EditStudentComponent,
    LoginComponent,
    AdminLogoutComponent,
    AdminDashboardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    MatTableModule,
    MatAutocompleteModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,

    RouterModule.forRoot([
      {
        path: 'School',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Add', component: SchoolComponent, canActivate: [AdminAuthGuardService] },
          { path: 'Edit/:schoolId', component: EditSchoolComponent, canActivate: [AdminAuthGuardService] },
          { path: 'All', component: AllSchoolComponent, canActivate: [AdminAuthGuardService] }
        ]
      },
      {
        path: 'Student',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Add', component: StudentMasterComponent, canActivate: [AdminAuthGuardService] },
          { path: 'Edit/:StudentID', component: EditStudentComponent, canActivate: [AdminAuthGuardService] },
          { path: 'All', component: AllStudentMasterComponent, canActivate: [AdminAuthGuardService] },
        ]
      },
      {
        path: 'Admin',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Dashboard', component: AdminDashboardComponent, canActivate: [AdminAuthGuardService] }

        ]
      },
      { path: 'Login', component: LoginComponent },
      { path: 'AdminLogout', component: AdminLogoutComponent },

      { path: '', redirectTo: "Login", pathMatch: 'full' },
      { path: '**', redirectTo: "Login", pathMatch: 'full' },


    ], { useHash: true })
  ],
  exports: [BsDatepickerModule],
  providers: [DatePipe, AdminAuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
